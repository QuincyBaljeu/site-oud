var userChoice = prompt("steen, papier of schaar?"); /*stelt de vraag in de alert-popup*/
var computerChoice = Math.random(); /*genereert een willekeurig getal*/

console.log(computerChoice);

if (computerChoice <= .34) { /*als het getal tussen 0 en 0.34 is, is computerChoice steen*/
    computerChoice = "steen"
}
else if (computerChoice > .34 && computerChoice < .67) { /*als het getal tussen 0.34 en 0.67 is, is computerChoice papier*/
    computerChoice = "papier"
}
else if (computerChoice > .67 && computerChoice < 1) { /*als het getal tussen 0.67 en 1 is, is computerChoice schaar*/
    computerChoice = "schaar"
}

function compare(choice1, choice2) { /*functie die userChoice en computerChoice vergelijkt*/

    if (choice1 === choice2) { /*als choice1 en choice2 gelijk zijn, is het gelijkspel*/
        alert("Gelijkspel!");
    }

    else if (choice1 === "steen") { /*als choice1 steen is en choice2 schaar is wint steen, als choice2 papier is, wint papier*/
        if (choice2 === "schaar") {
            alert("Steen wint!");
        }
        else {
            alert("Papier wint!");
        }

    }
    else if (choice1 === "papier") { /*als choice1 papier is en choice2 steen wint papier, als choice2 schaar is wint schaar*/
        if (choice2 === "steen") {
            alert("Papier wint!");
        }
        else {
            alert("Schaar wint!");
        }
    }
    else if (choice1 === "schaar") { /*als choice1 schaar is en choice2 steen wint steen, als choice2 papier is wint schaar*/
        if (choice2 === "steen") {
            alert("Steen wint");
        }
        else {
            alert("Schaar wint!")
        }
    }
};


compare(userChoice, computerChoice);